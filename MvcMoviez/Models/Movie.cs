﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace MvcMoviez.Models
{
    public class Movie
    {
        [Required]
        public int ID { get; set; }
        [Required]
        public string  Title  { get; set; }
        [DisplayFormat(DataFormatString= "{0:d}")]
        public DateTime  ReleaseDate  { get; set; }
        [Required]
        public string  Genre { get; set; }
        [StringLength(5)]
        public string Rating { get; set; }
        [Range(1,20)]
        [DataType(DataType.Currency)]
        public Decimal Price { get; set; }
      
    }

    public class MovieDBContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
    }
}